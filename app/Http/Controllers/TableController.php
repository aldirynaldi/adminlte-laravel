<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TableController extends Controller
{
    public function Table(){
        return view('items.table');
    }

    public function DataTable(){
        return view('items.datatable');
    }
}
